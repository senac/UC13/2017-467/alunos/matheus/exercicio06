/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.Teste;

import br.com.senac.SaladeAula.Aluno;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class TesteNota {
    
    public TesteNota() {
    }
    @Test
    public void DeveRetornaAprovado (){
        Aluno situacao = new Aluno ();
        int numero = 9;
        boolean resultado = situacao.nota(numero);
        
        assertTrue(resultado);
    }

    public void DeveRetornaReprovado (){
        Aluno situacao = new Aluno ();
        int numero = 5;
        boolean resultado = situacao.nota(numero);
        assertTrue(resultado);
        
    }

    
}
